# DockHa - Hadoop Cluster on Docker Swarm
# Copyright (C) 2016 Jan Czogalla, TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#!/bin/bash
#
_e=0
_prg="$( basename ${0} )"

DATE="/bin/date"
DOCKER="/usr/bin/docker"
GETOPT="/usr/bin/getopt"

SCRIPTDIR="$( dirname "${0}" )"
if [ -f ${SCRIPTDIR}/bashlib.sh ]; then
  . ${SCRIPTDIR}/bashlib.sh
  _e=${?}
  if [ ${_e} -ne 0 ]; then
     echo "Error (${_e}) while loading bashlib.sh." >&2
     exit ${_e}
  fi
else
  echo "Error: bashlib.sh does not exist." >&2
  exit 1
fi

#
# usage () 
#
usage () {
  local _e=0
  cat >&2 <<EOF
${_prg} -h # this help
${_prg} [options] create|add [<slave_count>]
${_prg} [options] stop|restart|shutdown|recreate|clean [<container>...]

options:
  -i | --image= The image to start the containers with
  -o | owner=\$USER The owner of the cluster, used in the name of the container. Defaults to \$USER
  -p | --properties=hdc.properties Property file for the hadoop cluster. Defaults to hdc.properties
  Image and owner options can be set in the properties file but can be overriden by command line parameters.

commands:
  create|add Creates <slave_count> slaves for the hadoop cluster. May start a master container.
  stop Stops all or specified containers
  restart Restarts stopped all or specified containers
  shutdown Removes all or specified containers. Writes a file containing information about shut down containers
  recreate Starts new containers on the basis of the file written by shutdown
  clean Removes all or specified containers and cleans the data associated with these containers
EOF
  return ${_e}
}

function main() {
local _param=""
set_environment "_param" ${@}
eval set -- ${_param}

CONTAINER_OPTS="${USER_OPTS} -dP --net ${_docker_net} -e JAVA_TOOL_OPTIONS=-Dmaster.host=${_master_name}"
# check for mount point
if [ ${_mount_point} ]; then
  CONTAINER_OPTS="${CONTAINER_OPTS} -v ${_mount_point}:/data" #TODO mount point+container name?
else
  log_warning "No mount point was specified. Data will be lost if containers are removed!"
fi

MASTER_OPTS="${CONTAINER_OPTS} --cpu-shares ${_master_cpus} -m ${_master_memory}"
SLAVE_OPTS="${CONTAINER_OPTS} --cpu-shares ${_slave_cpus} -m ${_slave_memory}"

if [ "${_allow_multiple}" != true ]; then
  SLAVE_OPTS="${SLAVE_OPTS} -e affinity:container!=${_slave_base_name}*"
  SLAVE_OPTS="${SLAVE_OPTS} -e affinity:container!=${_master_name}"
fi

# parse cmd
_cmd=${1}
case ${_cmd} in
  create|add)
    network_create
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      exit ${_e}
    fi
    hdc_create $2
    _e=${?}
  ;; # create new slaves
  stop|restart|shutdown|clean)
    shift
    _containers=""
    check_containers "_containers" all ${@}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      exit ${_e}
    fi
    hdc_${_cmd} ${_containers}
    _e=${?}
  ;; # stop|restart|shutdown|clean specified containers (shift, $@)
  recreate)
    hdc_create "recreate"
    _e=${?}
  ;; # recreate from shutdown file
  *)
    _e=1
    log_error "(${_e}) unknown command ${1}." # unknown command
    usage
    ;;
esac
exit ${_e}
}

main ${@}
