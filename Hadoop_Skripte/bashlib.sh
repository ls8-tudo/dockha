# DockHa - Hadoop Cluster on Docker Swarm
# Copyright (C) 2016 Jan Czogalla, TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#bashlib.sh
#
# log ( message ...  )
#
log () {
  local _dv
  _dv="$( ${DATE} '+%F_%H:%M:%S' )"
  # ${LOGGER} -t ${_prg} "${@}"  
  echo "${_dv} ${_prg}: ${@}" >&2
}

#
# log_debug ( message ...  )
#
log_debug() {
  log "DEBUG: " "$@"  
}

#
# log_info ( message ...  )
#
log_info () {
  log "INFO: " "$@"  
}

#
# log_warning ( message ...  )
#
log_warning () {
  log "WARNING: " "$@"  
}

#
# log_error ( message )
#
log_error () {
  log "ERROR: " "$@"  
}

# -----------------------------------
# hdc and docker specific functions
# -----------------------------------

#
# set_environment(pvar, args...)
#
function set_environment(){
  local _pval=${1}
  local _e=0
  local _erg=0
  shift
  # parse switches
  _shortopts="ho:i:p:"
  _longopts="help,owner:,image:,properties:"
  PARSED_ARGS="$( ${GETOPT} -n "${_prg}" -o "${_shortopts}" -l "${_longopts}"  -- "${@}" )"
  _erg=${?}

  log_debug "PARSED_ARGS:${PARSED_ARGS}"

  if [ ${_erg} -ne 0 ]; then
    log_error "Usage error (use -h for help)"
    usage
    exit 2
  fi

  _help=0
  _cluster_owner=""
  _image_url=""
  _prop_file=""

  eval set -- $PARSED_ARGS
  # evaluate switches
  local key
  while [[ $# > 0 ]]; do
    key="$1"
    case $key in
      -h|--help)
        usage
        exit 3;
        ;;
      -o|--owner)
        [ -z "${_cluster_owner}" ] || {
          _e=${?}
          log_error "(${_e}) double usage off parameter -o ${2}  already set to ${_cluster_owner}."
          usage
          exit ${_e}
        }
        _cluster_owner="${2}"
        shift
        ;;
      -i|--image)
        [ -z "${_image_url}" ] || {
          _e=${?}
          log_error "(${_e}) double usage off parameter -i ${2}  already set to ${_image_url}."
          usage
          exit ${_e}
        }
        _image_url="${2}"
        shift
        ;;
      --)
        shift
        break
        ;;
      -p|--properties)
        [ -z "${_prop_file}" ] || {
          _e=${?}
          log_error "(${_e}) double usage off parameter -p ${2}  already set to ${_prop_file}."
          usage
          exit ${_e}
        }
        _prop_file="${2}"
        shift
        ;;
      *)
        _e=1
        log_error "(${_e}) unknown parameter ${1}."     # unknown option
        usage
        exit ${_e}
        ;;
    esac
    shift
  done

  # load properties from file
  if [ -z ${_prop_file} ]; then
    _prop_file="${SCRIPTDIR}/hdc.properties"
  fi
  if [ -f ${_prop_file} ]; then
    . ${_prop_file}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      log_error "Error (${_e}) while loading property file ${_prop_file}."
      exit ${_e}
    fi
  else
    log_error "Error: Property file ${_prop_file} does not exist."
    exit 1
  fi

  # check for docker host
  if [ -z ${DOCKERHOST} ]; then
    if [ -z ${DOCKER_HOST} ]; then
      log_error "Neither the property nor the environment variable for the docker host is set."
      exit 1
    fi
    DOCKERHOST=${DOCKER_HOST}
  fi
  DOCKER="${DOCKER} -H ${DOCKERHOST}"

  # check for cluster owner
  if [ -z ${_cluster_owner} ]; then
    if [ -z ${_owner} ]; then
      log_error "Neither the property nor the parameter for the cluster owner is set."
      exit 1
    fi
  else
    _owner=${_cluster_owner}
  fi

  # check for container image
  if [ ${_image_url} ]; then
    _image=${_image_url}
  fi

  _cluster_base_name="${_owner}-${_base_name}"
  _master_name="${_cluster_base_name}-master"
  _slave_base_name="${_cluster_base_name}-slave-"
  _shutdown_file="${SCRIPTDIR}/${_cluster_base_name}.shutdown"

  # check for docker net or do default
  if [ -z ${_docker_net} ]; then
    _docker_net="${_cluster_base_name}-net"
  fi

  # set rest of parameters
  eval "${_pval}=\"${@}\""
}

#
# network_create()
#
function network_create(){
  local _e=0
  log_info "Checking if network needs to be created."
  case ${_docker_net} in
    host|bridge|none|container:*)
      return 0
          ;;
        *)
          ;;
  esac
  local _net_exists=""
  # newer docker version?
  #_net_exists=$($DOCKER network ls -qf name=${_docker_net})
  $DOCKER network inspect ${_docker_net} &>/dev/null
  _net_exists=${?}
  if [ ${_net_exists} -ne 0 ]; then
    log_info "Creating new overlay network ${_docker_net}."
    $DOCKER network create -d overlay ${_docker_net} >&2
    _e=${?}
    if [ ${_e} -eq 0 ]; then
      log_info "Created new overlay network ${_docker_net}."
    else
      log_error "Error while creating network ${_docker_net} (${_e})"
    fi
  else
    log_info "Network ${_docker_net} does exist."
  fi
  return ${_e}
}

#
# hdc_create(slave_count)
# hdc_create("recreate")
#
function hdc_create(){
  local _e=0
  local _param=${1}
  local _slave_count=0
  if [ ${_param} ]; then
    if [ ${_param} -eq ${_param} ] 2>/dev/null; then
      if [ ${_slave_count} -lt 0 ]; then
        log_warning "Negative numbers not allowed. Setting to 0."
      else
        _slave_count=${_param}
      fi
      _param=""
    elif [ ${_param} != recreate ]; then
      log_error "Invalid parameter ${_param}. Must be a number or \"recreate\""
      return 1
    fi
  else
    log_warning "No parameter specified. Assuming slave count as 0."
    _param=""
  fi

  local _shutdown_containers=""
  load_shutdown_file "_shutdown_containers"

  # start master if not already existent
  local _master_exists=""
  local _master_running=""
  check_containers "_master_exists" all ${_master_name}
  check_containers "_master_running" running ${_master_name}
  if [ -z ${_master_exists} ]; then
    docker_run ${_master_name} "${MASTER_OPTS}" master ${_shutdown_containers}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
     exit ${_e}
    fi
  elif [ -z ${_master_running} ]; then
    log_warning "Master container \"${_master_name}\" exists but is not running."
    hdc_restart ${_master_name}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      exit ${_e}
    fi
  fi
  
  if [ -z ${_param} ] && [ ${_slave_count} -eq 0 ]; then
    return 0
  fi
  
  if [ ${_param} ]; then
    log_info "Recreating containers from file ${_shutdown_file}"
  else
    log_info "Starting ${_slave_count} new slaves."
  fi
  
  # starting first slave on master container if allowed
  if [ -z ${param} ] && [ "${_allow_on_master}" = true ]; then
    log_info "Starting slave on container ${_master_name}."
    docker_exec '${HADOOP_CONF_DIR}/bootstrap.sh slave' ${_master_name}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      exit ${_e}
    fi
    _slave_count=$((${_slave_count}-1))
  fi
  
  # start slaves
  local _i
  local _id=1
  local _slave_name=""
  local _slaves_exist=""
  local _slave_names=""
  local _slaves_shutdown=""
  if [ -z ${_param} ]; then
    check_containers "_slaves_exist" all all
    # collect slave names
    for _i in $(seq 1 ${_slave_count}); do
      while [ true ]; do
        _slave_name="${_slave_base_name}${_id}"
        if [[ ! "${_slaves_exist}" =~ .*"${_slave_name}".* ]]; then
          if [[ "${_shutdown_containers}" =~ .*"${_slave_name}".* ]]; then
            _slaves_shutdown="${_slaves_shutdown} ${_slave_name}"
          else
            _slave_names="${_slave_names} ${_slave_name}"
          fi
          break
        fi
        _id=$((${_id}+1))
      done
      _id=$((${_id}+1))
    done
  else
    # recreate
    _slaves_shutdown=$(awk -v rgx="^.*master$" '{for (i=1; i <= NF; i++) if ($i !~ rgx) printf $i " " }' <<< ${_shutdown_containers})
  fi
  for _slave_name in ${_slaves_shutdown}; do
    docker_run ${_slave_name} "${SLAVE_OPTS}" slave ${_shutdown_containers}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      exit ${_e}
    fi
  done
  for _slave_name in ${_slave_names}; do
    docker_run ${_slave_name} "${SLAVE_OPTS}" slave ${_shutdown_containers}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      exit ${_e}
    fi
  done
  if [ -f ${_shutdown_file} ]; then
    remove_shutdown_information ${_master_name} ${_slaves_shutdown}
  fi
  return 0
}

#
# run(opts, name, cmd, shutdown_containers...)
#
function docker_run(){
  local _e=0
  local _name=${1}
  local _opts="${2}"
  local _sdo=""
  local _cmd=${3}
  shift 3
  if [ -z ${_image} ]; then
    log_error "Neither the property nor the parameter for the container image is set."
    exit 1
  fi
  _shutdown_containers=${@}
  _opts="${_opts} --name ${_name}"
  if [[ "${docker_net}" != "host" ]]; then
    _opts="${_opts} -h ${_name}"
  fi
  get_shutdown_opts "_sdo" ${_name} ${_shutdown_containers}
  _opts="${_opts} ${_sdo}"
  log_info "Starting container ${_name}"
  $DOCKER "run" ${_opts} ${_image} ${_cmd} >&2
  _e=${?}
  if [ ${_e} -eq 0 ]; then
    log_info "Started container ${_name}."
  else
    log_error "Error while starting container ${_name} (${_e})."
  fi
  return ${_e}
}

#
# hdc_stop(containers...)
#  
function hdc_stop(){
  docker_cmd " " "stop -t 1" ${@}
}

#
# hdc_restart(containers...)
#  
function hdc_restart(){
  docker_cmd " " restart ${@}
}

#
# hdc_remove(containers...)
#  
function hdc_remove(){
  docker_cmd " " "rm -f" ${@}
}

#
# hdc_shutdown(containers...)
#  
function hdc_shutdown(){
  local _containers=${@}
  check_containers "_containers" "all full" ${_containers}
  write_shutdown_file ${_containers}
  hdc_stop ${_containers}
  hdc_remove ${_containers}
}

#
# docker_inspect(pvar, format, containers...)
#
function docker_inspect(){
  local _e=0
  local _pvar=${1}
  local _format=${2}
  shift 2
  local _lcontainers=${@}
  docker_cmd ${_pvar} "inspect --format=\"${_format}\"" ${_lcontainers}
  _e=${?}
  return ${_e}
}

#
# hdc_clean(containers...)
#
function hdc_clean(){
  local _e=0
  if [ -z ${_mount_point} ]; then
    log_warning "Nothing to clean, no mount point specified."
    return 3
  fi
  if [ ${#} -eq 0 ]; then
    log_warning "No container(s) specified to clean. Doing nothing."
    return 3
  fi
  local _containers=${@}
  local _containers_stopped=""
  local _containers_running=""
  check_containers "_containers_stopped" stopped ${_containers}
  check_containers "_containers_running" running ${_containers}
  if [ "${_containers_running}" ]; then
    docker_exec '$HADOOP_CONF_DIR/bootstrap.sh clean' ${_containers_running}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      return ${_e}
    fi
  fi
  if [ "${_containers_stopped}" ]; then
    clean_nodes ${_containers_stopped}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      return ${_e}
    fi
  fi
  check_containers "_containers_running" running ${_containers}
  if [ "${_containers_running}" ]; then
    hdc_stop ${_containers_running}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      return ${_e}
    fi
  fi
  check_containers "_containers" all ${_containers}
  if [ "${_containers}" ]; then
    hdc_remove ${_containers}
    _e=${?}
  fi
  return ${_e}
}

#
# clean_nodes(containers...)
#
function clean_nodes(){
  local _e=0
  local _res=0
  if [ ${#} -eq 0 ]; then
    log_warning "No container(s) specified for cleaning. Doing nothing."
    return 3
  fi
  local _i=0
  local _containers=${@}
  local _container=""
  local _cleaner_name="${_cluster_base_name}-cleaner"
  local _opts="--cpu-shares 1 -m 2g --rm -it "
  local _cleaner_opts=""
  local _nodes=""
  local _node=""
  docker_inspect "_nodes" "{{.Node.Name}}" ${_containers}
  _nodes=(${_nodes})
  # remove containers beforehand to free resources
  hdc_remove ${_containers_stopped}
  for _container in ${_containers}; do
    _cleaner_opts="${_opts} -e constraint:node==${_nodes[${_i}]} -v ${_mount_point}:/data" #TODO mount point?
    docker_run ${_cleaner_name} "${_cleaner_opts}" clean
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      _res=$((${_res}+1))
    fi
    _i=$((${_i}+1))
  done
  return ${_res}
}

#
# docker_cmd(pvar, cmd, containers...)
#
function docker_cmd(){
  local _e=0
  local _pvar=${1}
  local _lval="$(echo \$${_pvar})"
  if [ ${_lval} = "$" ]; then
    _pvar=""
  fi
  _lval=""
  local _cmd=${2}
  if [ -z "${_cmd}" ]; then
    log_error "No command specified for function docker_cmd."
    return 1
  fi
  shift 2
  local _lcontainers="${@}"
  if [ -z "${_lcontainers}" ]; then
    log_warning "No container(s) specified for docker ${_cmd}. Doing nothing."
    return ${_e}
  fi
  local _cmd_desc=""
  case "${_cmd}" in
    stop*)
      _cmd_desc="Stopping"
    ;;
    restart)
      _cmd_desc="Restarting"
    ;;
    rm*)
      _cmd_desc="Removing"
    ;;
    inspect*)
      _cmd_desc="Inspecting"
    ;;
    *)
    ;;
  esac
  log_info "${_cmd_desc} container(s):"$'\n'"${_lcontainers}"
  _lval=$($DOCKER ${_cmd} ${_lcontainers})
  _e=${?}
  if [ ${_e} -eq 0 ]; then
    case ${_cmd} in
      stop*)
        _cmd_desc="Stopped"
      ;;
      restart)
        _cmd_desc="Restarted"
      ;;
      rm*)
        _cmd_desc="Removed"
      ;;
      inspect*)
      _cmd_desc="Inspected"
      ;;
      *)
      ;;
    esac
    log_info "${_cmd_desc} container(s):"$'\n'"${_lcontainers}" #TODO line break in log?
    if [ ${_pvar} ]; then
      eval "${_pvar}=\"${_lval}\""
    fi
  else
    _cmd_desc=$(tr '[:upper:]' '[:lower:]'<<<${_cmd_desc})
    log_error "Error (${_e}) while ${_cmd_desc} container(s):"$'\n'"${_lcontainers}."
  fi
  return ${_e}
}

#
# docker_exec(cmd, containers...)
#
function docker_exec(){
  local _e=0
  local _res=0
  local _cmd="${1}"
  if [ -z "${_cmd}" ]; then
    log_error "No command specified for function docker_exec."
    return 1
  fi
  shift
  local _containers="${@}"
  local _container=""
  if [ -z "${_containers}" ]; then
    log_warning "No container(s) specified for execution. Doing nothing."
    return ${_e}
  fi
  for _container in ${_containers}; do
    log_info "Executing ${_cmd} on container ${_container}."
    $DOCKER exec ${_container} bash -c "${_cmd} &>/dev/null" >&2
    _e=${?}
    if [ ${_e} -eq 0 ]; then
      log_info "Executed ${_cmd} on container ${_container}."
    else
      log_error "Error (${_e}) while executing ${_cmd} on container ${_container}."
      _res=$((${_res}+1))
    fi
  done
  return ${_res}
}

#
# check_containers(pvar, type, containers...)
#
function check_containers(){
  local _e=0
  local _res=0
  local _pvar=${1}
  local _type=${2}
  shift 2
  local _lval="${@}"
  local _tmp=""
  local _container=""
  local _candidates=""
  local _insp_containers=""
  local _full_name="false"
  local _ps_param="--format={{.Names}}"
  case ${_type} in
    all*)
      _ps_param="${_ps_param} -a"
    ;;
    running*) # changes nothing; default for ps is running
    ;;
    stopped*)
      _ps_param="${_ps_param} -a -f status=exited"
    ;;
    *)
      log_error "${_type} is not a correct type. Must be all|running|stopped [full]"
      return 3
    ;;
  esac
  case ${_type} in
    *full)
      _full_name="true"
    ;;
    *)
    ;;
  esac
  _ps_param="${_ps_param} -f name="
  # get full container names (with host machine)
  if [ -z "${_lval}" ] || [ "${_lval}" = "all" ]; then
    _lval="$($DOCKER ps ${_ps_param}${_cluster_base_name})"
    _res=${?}
  else
    _candidates=${_lval}
    _lval=""
    for _container in ${_candidates}; do
      _tmp="$($DOCKER ps ${_ps_param}${_container})"
      _e=${?}
      if [ "${_tmp}" ]; then
        _lval="${_lval} ${_tmp}"
        if [ ${_e} -ne 0 ]; then
          _res=$((${_res}+1))
        fi
      fi
    done
  fi
  # get container names only
  if [ "${_lval}" ] && [ ${_full_name} = false ]; then
    docker_inspect "_insp_containers" {{.Name}} ${_lval}
    _res=${?}
    _lval=$(tr -d "/" <<< ${_insp_containers})
  fi
  if [ ${_res} -ne 0 ]; then
    log_error "${_res} errors while getting containers."
    _e=1
# TODO: warning or not?
#  elif [ -z ${_containers} ]; then
#    log_warning "No containers found matching ${_type} ${_cluster_base_name}. Doing nothing."
#    _e=3
  else
    eval "${_pvar}=\"${_lval}\""
  fi
  return ${_e}
}

#
# load_shutdown_file(pvar)
#
function load_shutdown_file(){
  local _e=0
  local _pvar=${1}
  local _lval=""
  if [ -f ${_shutdown_file} ]; then
    _lval="$(awk -F "/" '{print $2}' ${_shutdown_file})"
    _e=${?}
    eval "${_pvar}=\"${_lval}\""
    if [ ${_e} -eq 0 ]; then
      log_info "Loaded shutdown information from file ${_shutdown_file}."
    else
      log_warning "Could not read from shutdown file ${_shutdown_file}. Ignoring."
    fi
  else
    log_info "No shutdown information available."
  fi
  return ${_e}
}

#
# remove_shutdown_information(containers...)
#
function remove_shutdown_information(){
  local _e=0
  if [ ${#} -eq 0 ]; then
    log_info "No shutdown information to remove."
    return ${_e}
  fi
  local _containers=${@}
  local _shutdown_information=""
  for _container in ${_containers}; do
    _shutdown_information="${_shutdown_information} ${_container} "
  done
  write_shutdown_file ${_shutdown_information}
  _e=${?}
  return ${_e}
}

#
# write_shutdown_file(node/container...)
#
function write_shutdown_file(){
  local _e=0
  if [ ${#} -eq 0 ]; then
    log_info "Nothing to write to shutdown file."
    return ${_e}
  fi
  local _new_entries="${@}"
  local _old_entries=""
  local _entry_count=0
  if [ -f ${_shutdown_file} ]; then
    _old_entries="$(cat ${_shutdown_file})"
    _e=${?}
  else
    touch ${_shutdown_file}
    _e=${?}
  fi
  if [ ${_e} -ne 0 ]; then
    log_error "Error while accessing file ${_shutdown_file}."
    return ${_e}
  fi
  log_info "Writing shutdown information to ${_shutdown_file}."
  local _name=""
  local _node=""
  # write old enrties
  for _old_entry in ${_old_entries}; do
    _name=$(awk -F "/" '{print $2}' <<< ${_old_entry})
    if [[ ! "${_new_entries}" =~ .*"${_name}".* ]]; then
      echo ${_old_entry} >> ${_shutdown_file}
      _e=${?}
      if [ ${_e} -ne 0 ]; then
        log_error "Error while accessing file ${_shutdown_file}."
        return ${_e}
      fi
      _entry_count=$((${_entry_count}+1))
    fi
  done
  # write new entries
  for _new_entry in ${_new_entries}; do
    _name=$(awk -F "/" '{print $2}' <<< ${_new_entry})
    _node=$(awk -F "/" '{print $1}' <<< ${_new_entry})
    if [ "${_name}" ]; then
      echo ${_new_entry} >> ${_shutdown_file}
      _e=${?}
      if [ ${_e} -ne 0 ]; then
        log_error "Error while accessing file ${_shutdown_file}."
        return ${_e}
      fi
      _entry_count=$((${_entry_count}+1))
    fi
  done
  if [ ${_entry_count} -eq 0 ]; then
    log_info "No entries to write. Removing shutdown file."
    rm -f ${_shutdown_file}
    _e=${?}
  else
    log_info "Done writing shutdown file."
  fi
  return ${_e}
}

#
# get_shutdown_opts(pvar, container_name, shutdown_containers...)
#
function get_shutdown_opts(){
  local _e=0
  local _pvar=${1}
  local _lval=""
  local _name=${2}
  if [ -z ${_name} ]; then
    log_warning "No name specified to check for shutdown information"
    return 3
  fi
  shift 2
  local _shutdown_containers=${@}
  if [ -z "${_shutdown_containers}" ]; then
    return ${_e}
  fi
  if [[ "${_shutdown_containers}" =~ .*"${_name}".* ]]; then
    _lval=$(awk -F "/" -v rgx="^${_name}$" '\
      $2 ~ rgx {print "-e contraint:node=="$1};\
      ' ${_shutdown_file})
    _e=${?}
    eval "${_pvar}=\"${_lval}\""
  fi
  return ${_e}
}
