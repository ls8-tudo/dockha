# DockHa - Hadoop Cluster on Docker Swarm
# Copyright (C) 2016 Jan Czogalla, TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#!/bin/bash
#
_e=0
_prg="$( basename ${0} )"

DATE="/bin/date"
DOCKER="/usr/bin/docker"
GETOPT="/usr/bin/getopt"

SCRIPTDIR="$( dirname "${0}" )"
if [ -f ${SCRIPTDIR}/build_docker.properties ]; then
  . ${SCRIPTDIR}/build_docker.properties
  _e=${?}
  if [ ${_e} -ne 0 ]; then
     echo "Error (${_e}) while loading build_docker.properties."
     exit ${_e}
  fi
else
  echo "Error: build_docker.properties does not exist." >&2
  exit 1
fi

if [ -f ${SCRIPTDIR}/bashlib.sh ]; then
  . ${SCRIPTDIR}/bashlib.sh
  _e=${?}
  if [ ${_e} -ne 0 ]; then
     echo "Error (${_e}) while loading bashlib.sh." >&2
     exit ${_e}
  fi
else
  echo "Error: bashlib.sh does not exist." >&2
  exit 1
fi

if [ -z ${DOCKERHOST} ]; then
  log_error "Docker host was not specified."
  exit 1
fi

DOCKER="${DOCKER} -H ${DOCKERHOST}"

#
# usage () 
#
usage () {
  local _e=0
  cat >&2 <<EOF

${_prg} -h | -u <repository-user> -n <image-name> -v <image-version>
  -h | --help # this help
  -u | --user= <repository-user>
  -n | --name= <image-name>
  -v | --version <image-version>

EOF

  return ${_e}
}

#
# clean_reg()
#
function clean_reg(){
  local _img_id
  local _e
  if [ "${_image_url}" ]; then
    _img_id="$($DOCKER  images -q ${_image_url} )"
    if [ -n "${_img_id}" ]; then
      log_info "cleaning up images ${_img_id}"
      $DOCKER rmi -f ${_img_id}
      _e=${?}
      if [ ${_e} -ne 0 ]; then
      	log_warning "Error (${_e}) while removing image(s) ${_img_id}"
      	return ${_e}
      fi
    fi
  fi

  _img_id="$($DOCKER images -q ${_image_full_name})"
  if [ -n "${_img_id}" ]; then
    log_info "cleaning up images ${_img_id}"
    $DOCKER rmi -f ${_img_id}
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      log_warning "Error (${_e}) while removing image(s) ${_img_id}"
      return ${_e}
    fi
  fi

  log_info "cleanup ${_image_name}:${_image_version} done."
  return 0
}

_shortopts="hu:n:v:"
_longopts="help,user:,name:,version:"
PARSED_ARGS="$( ${GETOPT} -n "${_prg}" -o "${_shortopts}" -l "${_longopts}"  -- "${@}" )"
_erg=${?}

log_debug "PARSED_ARGS:${PARSED_ARGS}"

if [ ${_erg} -ne 0 ]; then
  log_error "Usage error (use -h for help)"
  usage
  exit 2
fi

_help=0
_repository_user=""
_image_name=""
_image_version=""
_image_url=""

eval set -- $PARSED_ARGS
while [[ $# > 0 ]]; do
  key="$1"
  case $key in
    -h|--help)
      usage
      exit 3;
      ;;
    -n|--name)
      [ -z "${_image_name}" ] || {
        _e=${?}
	log_error "(${_e}) double usage off parameter -n ${2}  already set to ${_image_name}."
        usage
        exit ${_e}
      }
      _image_name="${2}"
      shift
      ;;
    -v|--version)
      [ -z "${_image_version}" ] || {
        _e=${?}
	log_error "(${_e}) double usage off parameter -v ${2}  already set to ${_image_version}."
        usage
        exit ${_e}
      }
      _image_version="${2}"
      shift
      ;;
    -u|--user)
      [ -z "${_repository_user}" ] || {
        _e=${?}
        log_error "(${_e}) double usage off parameter -u ${2}  already set to ${_repository_user}."
        usage
        exit ${_e}
      }
      _repository_user="${2}"
      shift
      ;;
    --)
      ;;
    *)
      _e=1
      log_error "(${_e}) unknown parameter -u ${1}."     # unknown option
      usage
      exit ${_e}
      ;;
  esac
  shift
done

if [ -z "${_image_name}" -o -z "${_image_version}" -o -z "${_repository_user}" ]; then
  _e=1
  log_error "(${_e}) missing parameter name(${_image_name}), version(${_image_version}) and user(${_repository_user}) must not be empty."
  usage
  exit ${_e}
fi

_image_full_name="${_image_name}:${_image_version}"
if [ "${REPOSITORYHOST}" ] && [ "${_repository_user}" ]; then
  _image_url="${REPOSITORYHOST}/${_repository_user}/${_image_full_name}"
fi
clean_reg

if [ ${_e} -eq 0 ]; then
  ${DOCKER} build  -t "${_image_full_name}" "${SCRIPTDIR}"
  _e=${?}
  if [ ${_e} -ne 0 ]; then
    log_error "build(${_e}) ${_image__full_name} failed."
  elif [ "${_image_url}" ]; then
    ${DOCKER} tag -f "${_image_full_name}" \
           "${_image_url}"
    _e=${?}
    if [ ${_e} -ne 0 ]; then
      log_error "tag(${_e}) ${_image_full_name} " \
               "as ${_image_url} failed."
    else
      ${DOCKER} push "${_image_url}"
      _e=${?}
      if [ ${_e} -ne 0 ]; then
        log_error "push(${?}) ${_image_url} failed."
      else 
        ${DOCKER} pull "${_image_url}"
        _e=${?}
        if [ ${_e} -ne 0 ]; then
          log_error "${_image_url} failed"
        else
          log_info "${prg} create ${_image_url} succeed."
        fi
      fi
    fi
  fi
fi
exit ${_e}
