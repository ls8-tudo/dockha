# DockHa - Hadoop Cluster on Docker Swarm
# Copyright (C) 2016 Jan Czogalla, TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#bashlib.sh
#
# log ( message ...  )
#
log () {
  local _dv
  _dv="$( ${DATE} '+%F_%H:%M:%S' )"
  # ${LOGGER} -t ${_prg} "${@}"  
  echo "${_dv} ${_prg}: ${@}" >&2
}

#
# log_debug ( message ...  )
#
log_debug() {
  log "DEBUG: " "$@"  
}

#
# log_info ( message ...  )
#
log_info () {
  log "INFO: " "$@"  
}

#
# log_warning ( message ...  )
#
log_warning () {
  log "WARNING: " "$@"  
}

#
# log_error ( message )
#
log_error () {
  log "ERROR: " "$@"  
}
