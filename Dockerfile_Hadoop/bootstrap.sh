#!/bin/bash

# DockHa - Hadoop Cluster on Docker Swarm
# Copyright (C) 2016 Jan Czogalla, TU Dortmund Computer Science VIII
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#

function killtrap(){
    local _cmd=$1
    local _svscan_pid=$(pidof svscan)
    local _daemon=""
    # stop services
    if [ ${_svscan_pid} ]; then
       for _daemon in /etc/service/*; do
         svc -h ${_daemon}
         rm -f ${_daemon}
       done
       pkill -2 -P ${_svscan_pid}
    fi
    if [ -S hadoop_socks ]; then
      ssh -S hadoop_socks -o "StrictHostKeyChecking no" -O exit localhost &>/dev/null
    fi
    if [ ${_cmd} = clean ]; then
      rm -rf $DATA_DIR $HDFS_NAMENODEDIR
    fi
    if [ ${_svscan_pid} ]; then
      kill -2 ${_svscan_pid}
    fi
    #kill -2 -- -$(ps -o pgid= $(pidof svscan) | grep -o '[0-9]*') &>/dev/null
}

trap "killtrap" SIGINT SIGTERM

if [[ $1 == "bash" ]]; then
    exec /bin/bash
elif [[ $1 == "clean" ]]; then
    killtrap clean
else
    ENVDIR=/service/env
    if [ ! -d $ENVDIR ]; then
        mkdir $ENVDIR
        echo ${JAVA_TOOL_OPTIONS} > $ENVDIR/JAVA_TOOL_OPTIONS
        echo ${JAVA_HOME} > $ENVDIR/JAVA_HOME
        echo ${HADOOP_HOME} > $ENVDIR/HADOOP_HOME
        echo ${HADOOP_COMMON_HOME} > $ENVDIR/HADOOP_COMMON_HOME
        echo ${HADOOP_YARN_HOME} > $ENVDIR/HADOOP_YARN_HOME
        echo ${HADOOP_MAPRED_HOME} > $ENVDIR/HADOOP_MAPRED_HOME
        echo ${HDFS_USER} > $ENVDIR/HDFS_USER
        echo ${HDFS_GROUP} > $ENVDIR/HDFS_GROUP
        echo ${HADOOP_CONF_DIR} > $ENVDIR/HADOOP_CONF_DIR
        echo ${YARN_CONF_DIR} > $ENVDIR/YARN_CONF_DIR
        echo ${MAPRED_CONF_DIR} > $ENVDIR/MAPRED_CONF_DIR
        echo ${DATA_DIR} > $ENVDIR/DATA_DIR
        echo ${HDFS_NAMENODEDIR} > $ENVDIR/HDFS_NAMENODEDIR
    fi
    if [[ $1 == "master" ]]; then
        ln -s /service/hdfs-namenode /etc/service/hdfs-namenode
        ln -s /service/yarn-resourcemanager /etc/service/yarn-resourcemanager
        ln -s /service/mapred-historyserver /etc/service/mapred-historyserver
        # start socks proxy
        if [ ! "$(pidof sshd)" ]; then
            service ssh start
        fi
        while [ ! "$(pidof sshd)" ]; do
            sleep 1000
        done
        if [ ! -e hadoop_socks ]; then
          ssh -M -S hadoop_socks -fNT -o "StrictHostKeyChecking no" -D 0.0.0.0:1234 localhost &>/dev/null
        fi
    else
	ln -s /service/hdfs-datanode /etc/service/hdfs-datanode
	ln -s /service/yarn-nodemanager /etc/service/yarn-nodemanager
    fi

    if [ ! $(pidof svscan) ]; then
        /usr/bin/svscanboot
    fi
fi
